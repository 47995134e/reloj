﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reloj
{
    [Serializable()]
    class Alarma
    {

        private int h;
        private int min;
        private int secs;
        private Boolean activa;

        public Alarma(int h, int min, int secs, bool activa)
        {
            this.H = h;
            this.Min = min;
            this.Secs = secs;
            this.activa = activa;
        }

        public Alarma()
        {
        }

        public Boolean alarmaActiva { get; set; }
        public int H { get => h; set => h = value; }
        public int Min { get => min; set => min = value; }
        public int Secs { get => secs; set => secs = value; }
    }
}
